;ELC   
;;; Compiled
;;; in Emacs version 25.3.50.1
;;; with all optimizations.

;;; This file uses dynamic docstrings, first added in Emacs 19.29.

;;; This file does not contain utf-8 non-ASCII characters,
;;; and so can be loaded in Emacs versions earlier than 23.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(byte-code "\300\301\302\303\304\305\306\307&\210\310\311\312\313\304\305%\210\314\315\311\"\207" [custom-declare-variable slurm-script-directives-face 'slurm-script-directives "Face name to use for SBATCH directives in SLURM job submission scripts." :group slurm :type face custom-declare-face slurm-script-directives nil "Face to use for SBATCH directives in SLURM job submission scripts." copy-face font-lock-type-face] 8)
#@33 Keymap for `slurm-script-mode'.
(defvar slurm-script-mode-map (byte-code "\301 \302\303\304#\210)\207" [map make-sparse-keymap define-key "" slurm-script-insert-directive] 4) (#$ . 840))
#@62 List of allowed SBATCH keywords in SLURM submission scripts.
(defconst slurm-script-keywords '("account" "acctg-freq" "begin" "checkpoint" "checkpoint-dir" "comment" "constraint" "constraint" "contiguous" "cores-per-socket" "cpu-bind" "cpus-per-task" "dependency" "distribution" "error" "exclude" "exclusive" "extra-node-info" "get-user-env" "get-user-env" "gid" "hint" "immediate" "input" "job-id" "job-name" "licences" "mail-type" "mail-user" "mem" "mem-bind" "mem-per-cpu" "mincores" "mincpus" "minsockets" "minthreads" "network" "nice" "nice" "no-kill" "no-requeue" "nodefile" "nodelist" "nodes" "ntasks" "ntasks-per-core" "ntasks-per-socket" "ntasks-per-node" "open-mode" "output" "overcommit" "partition" "propagate" "propagate" "quiet" "requeue" "reservation" "share" "signal" "socket-per-node" "tasks-per-node" "threads-per-core" "time" "tmp" "uid" "wckey" "workdir" "wrap") (#$ . 1038))
#@79 Regular expression matching SBATCH keywords in a SLURM job
submission script.
(defconst slurm-script-keywords-re (byte-code "\301\302!\303Q\207" [slurm-script-keywords "--" regexp-opt "\\b"] 3) (#$ . 1940))
#@73 Interactively insert a SLURM directive of the form:

#SBATCH --keyword

(defalias 'slurm-script-insert-directive #[(keyword) "\301\302Qc\207" [keyword "#SBATCH --" " "] 3 (#$ . 2154) (list (completing-read "Keyword: " slurm-script-keywords nil t))])
#@272 Search for the next #SBATCH directive.

Returns:
- nil    if no SBATCH directives are found
- error  if the following SBATCH directive is malformed
- an integer corresponding to the point position of the next SBATCH
   directive beginning if it is found and well-formed.
(defalias 'slurm-search-directive-1 #[(limit) "\303\304\305#\2054 \306\224\303\307\n\310Q\305#\2032 \311\312!\204. \311\313!\203( \314 \210\202. \303\315\305#\210	\2023 \316)\207" [limit beg slurm-script-keywords-re re-search-forward "^\\s *#SBATCH\\b" t 0 "\\=\\s +" "\\s *" looking-at "\\(\\s<\\|$\\)" "\\s\"" forward-sexp "\\=[^[:space:]#\n]+" error] 4 (#$ . 2412)])
(defalias 'slurm-search-directive #[(limit) "\303\304 \305\216\306\n!\211\307=\204 *\205 \310`D!\210\311)\207" [beg save-match-data-internal limit nil match-data #[nil "\301\302\"\207" [save-match-data-internal set-match-data evaporate] 3] slurm-search-directive-1 error set-match-data t] 3])
#@103 Non-nil if Slurm-Script mode is enabled.
Use the command `slurm-script-mode' to change this variable.
(defvar slurm-script-mode nil (#$ . 3365))
(make-variable-buffer-local 'slurm-script-mode)
#@267 Edit SLURM job submission scripts.

When slurm-script-mode is on, SBATCH directives are highlighted.
This mode also provides a command to insert new SBATCH directives :
  \<slurm-script-mode-map>
  \[slurm-script-insert-directive] - `slurm-script-insert-directive'

(defalias 'slurm-script-mode #[(&optional arg) "\305 	\306=\203 \n?\202 \307	!\310V\311\n\203\" \312\313\"\210\202' \314\313\"\210)\315\316\n\2032 \317\2023 \320\"\210\321\322!\203X \305 \203G \305 \232\203X \323\324\325\n\203S \326\202T \327\f#\210))\330 \210\n\207" [#1=#:last-message arg slurm-script-mode kwlist local current-message toggle prefix-numeric-value 0 (("^\\s *\\(#SBATCH[^#\n]*\\)\\s *\\(#.*\\)?$" 1 font-lock-warning-face t) (slurm-search-directive 0 slurm-script-directives-face t)) font-lock-add-keywords nil font-lock-remove-keywords run-hooks slurm-script-mode-hook slurm-script-mode-on-hook slurm-script-mode-off-hook called-interactively-p any " in current buffer" message "Slurm-Script mode %sabled%s" "en" "dis" force-mode-line-update] 4 (#$ . 3565) (list (or current-prefix-arg 'toggle))])
#@182 Hook run after entering or leaving `slurm-script-mode'.
No problems result if this variable is not bound.
`add-hook' automatically binds it.  (This is true for all hook variables.)
(defvar slurm-script-mode-hook nil (#$ . 4664))
(byte-code "\301\302\303\304\211%\207" [slurm-script-mode-map add-minor-mode slurm-script-mode " slurm" nil] 6)
#@66 Turn slurm-mode on if SBATCH directives are found in the script.
(defalias 'turn-on-slurm-script-mode #[nil "\212eb\210\300d!\205 \301\302!)\207" [slurm-search-directive slurm-script-mode 1] 2 (#$ . 5012) nil])
(byte-code "\300\301\302\"\210\303\304!\207" [add-hook sh-mode-hook turn-on-slurm-script-mode provide slurm-script-mode] 3)
