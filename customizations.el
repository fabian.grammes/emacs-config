(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(aquamacs-customization-version-id 0 t)
 '(bmkp-last-as-first-bookmark-file
   "/Users/fabiangrammes/Library/Preferences/Aquamacs Emacs/Packages/bookmarks")
 '(custom-safe-themes
   (quote
    ("8aebf25556399b58091e533e455dd50a6a9cba958cc4ebb0aab175863c25b9a4" default)))
 '(one-buffer-one-frame-mode nil nil (aquamacs-frame-setup))
 '(package-selected-packages
   (quote
    (snakemake-mode ecb ess magit shell-pop s solarized-theme autopair fill-column-indicator diredful ## org rainbow-delimiters)))
 '(shell-pop-universal-key "C-t")
 '(tabbar-mode t nil (tabbar)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(org-block ((t (:background "#b0e0e6" :foreground "#ff6347"))))
 '(org-block-begin-line ((t (:foreground "#FFFFFF" :background "#6c7b8b"))))
 '(org-block-end-line ((t (:overline "#A7A6AA" :foreground "#FFFFFF" :background "#6c7b8b")))))
