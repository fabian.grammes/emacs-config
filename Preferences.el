;; This is the Aquamacs Preferences file.
;; Add Emacs-Lisp code here that should be executed whenever
;; you start Aquamacs Emacs. If errors occur, Aquamacs will stop
;; evaluating this file and print errors in the *Messages* buffer.
;; Use this file in place of ~/.emacs (which is loaded as well.)

(add-to-list 'load-path "~/Library/Preferences/Aquamacs Emacs/Packages/custom/")
(add-to-list 'package-archives
	     '("melpa" . "http://melpa.org/packages/"))

(load-theme 'solarized-dark t)

(require 'ls-lisp)
(setq ls-lisp-use-insert-directory-program t)
(setq insert-directory-program "/usr/local/Cellar/coreutils/8.30/libexec/gnubin/ls")
;(require 'dired-sort-map)
(setq dired-listing-switches "--group-directories-first -alh")


(require 'tramp)
(add-to-list 'tramp-remote-path 'tramp-own-remote-path)
(setq tramp-default-method "scp")

;; Formatting Style 

(defun my-ess-hook ()
    (smartscan-mode 1)
    (setq ess-nuke-trailing-whitespace-p nil)
    (setq ess-first-continued-statement-offset 4
          ess-continued-statement-offset 0)
    )
(defun my-postinit-ess ()
  "my ess init code run after package-initialize"
  (require 'ess)
  (require 'ess-site)
  (setq-default ess-dialect "R"
                inferior-R-args "--no-save "
                ess-nuke-trailing-whitespace-p nil
                ess-ask-for-ess-directory nil)
  (add-hook 'ess-mode-hook 'my-ess-hook)
  )
(add-hook 'after-init-hook 'my-postinit-ess)

;; When splitting vertically: copy file from dired1 to dired2
(setq dired-dwim-target t)

;;Coloring dired files
(require 'diredful)
(diredful-mode 1)
;; Use the following comands to set custom coloring
; M-x diredful-add
; M-x diredful-edit
; M-x diredful-delete

;; ibuffer
(defalias 'list-buffers 'ibuffer) ; make ibuffer default


;; Set R path
(setq exec-path (append exec-path '("/Library/Frameworks/R.framework/Resources/bin/R")))

;; Bookmarks Plus
;; clone from https://github.com/emacsmirror/bookmark-plus and then byte compile
(add-to-list 'load-path "~/Library/Preferences/Aquamacs Emacs/Packages/custom/bookmark-plus")
(require 'bookmark+)
(setq inhibit-splash-screen t)
(bookmark-bmenu-list)
(switch-to-buffer "*Bookmark List*")

;; MaGit installed at /usr/local/share/emacs/site-lisp
;;  Current magit versions are abysmal slow so much better
;;  to use an old version from 2016
(add-to-list 'load-path "/Users/fabiangrammes/Library/Preferences/Aquamacs Emacs/Packages/custom/magit-1.2.2")
(require 'magit)
(remove-hook 'server-switch-hook 'magit-commit-diff)

;; rainbow delimiters
(when (require 'rainbow-delimiters nil 'noerror)
  (add-hook 'scheme-mode-hook 'rainbow-delimiters-mode)
  (add-hook 'slime-mode-hook 'rainbow-delimiters-mode)
  (add-hook 'emacs-lisp-mode-hook 'rainbow-delimiters-mode)
  (add-hook 'python-mode-hook 'rainbow-delimiters-mode)
  (add-hook 'R-mode-hook 'rainbow-delimiters-mode)
  (add-hook 'org-mode-hook 'rainbow-delimiters-mode)
  (add-hook 'sh-mode-hook 'rainbow-delimiters-mode)
(add-hook 'sql-mode-hook 'rainbow-delimiters-mode))

;; Autopair brakets
(require 'autopair)
(autopair-global-mode 1)



;; ESSH
;; downloaded from https://www.emacswiki.org/emacs/essh.el
(add-to-list 'load-path "~/Library/Preferences/Aquamacs Emacs/Packages/custom/essh.el")
(require 'essh)                                                    ;;
(defun essh-sh-hook ()
  (define-key sh-mode-map "\C-c\C-o" 'pipe-region-to-shell)
  (define-key sh-mode-map "\C-c\C-b" 'pipe-buffer-to-shell)
  (define-key sh-mode-map "\C-c\C-j" 'pipe-line-to-shell)
  (define-key sh-mode-map "\C-c\C-n" 'pipe-line-to-shell-and-step)
  (define-key sh-mode-map "\C-c\C-f" 'pipe-function-to-shell))
  ;;  (define-key sh-mode-map "\C-c\C-d" 'shell-cd-current-directory))
(add-hook 'sh-mode-hook 'essh-sh-hook) 

;; Slurm cloned from https://github.com/ffevotte/slurm.el
;; requires installation of 's'
(add-to-list 'load-path "~/Library/Preferences/Aquamacs Emacs/Packages/custom/slurm")
(require 'slurm-mode)
(require 'slurm-script-mode)


(require 'snakemake-mode)

;; dimmer
(add-to-list 'load-path "~/Library/Preferences/Aquamacs Emacs/Packages/custom/dimmer.el-0.3.0")
(require 'dimmer)
(dimmer-mode)

;; shell pop
(require 'shell-pop)
(global-set-key (kbd "M-q") 'shell-pop)

;;==============================================================================
;;================== KEY BINDINGS ==============================================
;;==============================================================================
;;cycle buffers
(global-set-key (kbd "M-<right>") 'next-buffer)
(global-set-key (kbd "M-<left>") 'previous-buffer)

;; easy keys to split window. Key based on ErgoEmacs keybinding
; using the meta key to jump between windows
(global-set-key (kbd "M-1") 'split-window-horizontally)
(global-set-key (kbd "M-2") 'split-window-vertically)
(global-set-key (kbd "M-3") 'delete-window)
;(global-set-key (kbd "M-4") 'delete-other-windows)
(global-set-key (kbd "M-<tab>") 'other-window)

(defun split-3-windows-horizontally-evenly ()
  (interactive)
  (command-execute 'split-window-horizontally)
  (command-execute 'split-window-horizontally)
  (command-execute 'balance-windows)
)
(global-set-key (kbd "M-4") 'split-3-windows-horizontally-evenly)
(global-set-key (kbd "M-l") 'bookmark-bmenu-list)
; using the C-b to jump between windows
(global-set-key (kbd "C-b") 'switch-to-buffer)

;; Remote R function to run R on the cluster
;; specified ssh -X settings in the .ssh/config file
;; function to call shell and then type R
(defun remote-R ()
  (interactive)
  (shell)
  (insert "R")
  (comint-send-input))
; add keybinding for this (should be called when on remote)
(global-set-key (kbd "C-c R") 'remote-R)





;;==============================================================================
;;================== BUFFERS ===================================================
;;==============================================================================
;; ;; Highlight the current line
;; (global-hl-line-mode 1)
;; (set-face-background 'hl-line "grey65")

;; ;; Fill column indicator
(require 'fill-column-indicator)
(setq fci-rule-width 2)
(setq fci-rule-color "grey90")
(setq fci-rule-column 80)
(add-hook 'after-change-major-mode-hook 'fci-mode)


;;++++++++++++++ ORG MODE ++++++++++++++++++++++++++++++++++++++++++++++++++++++
(add-to-list 'load-path "~/Library/Preferences/Aquamacs Emacs/Packages/elpa/org-9.1.14")
(require 'org)


(setq org-src-fontify-natively nil)
(custom-set-faces
 '(org-block-begin-line
   ((t (:foreground "#FFFFFF" :background "#6c7b8b"))))
 '(org-block
   ((t (:background "#b0e0e6" :foreground "#ff6347"))))
 '(org-block-end-line
   ((t (:overline "#A7A6AA" :foreground "#FFFFFF" :background "#6c7b8b"))))
 )

;; stop C-c C-c within code blocks from querying
;; and stop automatic evaluation of blocks upon export
(setq org-confirm-babel-evaluate nil)
(setq org-export-babel-evaluate nil)

;; ;; Language support for ORG MODE
(org-babel-do-load-languages
 'org-babel-load-languages
  '( (R . t)
     (python . t)
     (sql . t)
     (emacs-lisp . t)
     (shell . t)
     (latex . t)
     (org . t)
     (sqlite . t)
))

(setq org-babel-R-command "/Library/Frameworks/R.framework/Resources/bin/R --no-save")
